<?php

function classRegister($class)
{
    if(file_exists('lib/' . $class . '.php'))
    {
        require_once './lib/' . $class . '.php';
    }
}

spl_autoload_register('classRegister');

?>