<?php

try
{
    require_once 'autoloader.php';

    if(!isset($argv[1], $argv[2]) || $argv[1] === '--help' || $argv[1] === '-h')
    {
        echo "The script posts an account to the Stalker Portal generating all required info from IP address " . PHP_EOL . PHP_EOL .
            "USAGE:" . PHP_EOL .
            "\t php " . basename(__FILE__) . " [property] ... [period] ..." . PHP_EOL . PHP_EOL .
            "PROPERTIES:" . PHP_EOL .
            "\t -ip" . PHP_EOL .
            "\t\t ip address" . PHP_EOL . PHP_EOL .
            "PERIODS:" . PHP_EOL .
            "\t -d, --days" . PHP_EOL .
            "\t\t integer number of days" . PHP_EOL .
            "\t -m, --months" . PHP_EOL .
            "\t\t integer number of months" . PHP_EOL .
            "EXAMPLES:" . PHP_EOL .
            "\t php " . basename(__FILE__) . " -ip 192.168.1.25 -d 7" . PHP_EOL .
            "\t\t add 192.168.1.25 for 7 days" . PHP_EOL .
            "\t php " . basename(__FILE__) . " -ip 192.168.1.25 -m 2" . PHP_EOL .
            "\t\t add 192.168.1.25 for 2 months" . PHP_EOL . PHP_EOL .
            "HELP:" . PHP_EOL .
            "\t -h, --help \t this help message" . PHP_EOL;
        exit;
    }

    /**
     * @param $key
     * @param array $args
     * @return mixed
     */
    function findArgument($key, array $args)
    {
        $i = array_search($key, $args);
        return $args[$i+1];
    }

    /**
     * @param StalkerPortal $portal
     * @return int|null
     * @throws StalkerPortalApiExeption
     */
    function generateUniqueLogin(StalkerPortal $portal)
    {
        if(!$portal->checkConnection())
        {
            throw new StalkerPortalApiExeption("No connection to Stalker Portal API server");
        }

        $login = null;
        do {
            $login = mt_rand(000001, 999999);
        } while(!$portal->isLoginUnique($login));

        return $login;
    }

    $ip = findArgument('-ip', $argv);

    $expireDate = null;
    if(in_array('-d', $argv))
    {
        $days = (int)findArgument('-d', $argv);
        $expireDate = date("Y-m-d H:i:s", strtotime("+ " . $days . " day"));
    }
    elseif (in_array('--days', $argv))
    {
        $days = (int)findArgument('--days', $argv);
        $expireDate = date("Y-m-d H:i:s", strtotime("+ " . $days . " day"));
    }
    elseif (in_array('-m', $argv))
    {
        $months = (int)findArgument('-m', $argv);
        $expireDate = date("Y-m-d H:i:s", strtotime("+ " . $months . " month"));
    }
    elseif (in_array('--months', $argv))
    {
        $months = (int)findArgument('--months', $argv);
        $expireDate = date("Y-m-d H:i:s", strtotime("+ " . $months . " month"));
    }
    else
    {
        exit("Set subscription period in days or months or run php " . basename(__FILE__) . "--help" . PHP_EOL);
    }

    $config = new Config();
    $conf = $config->get();

    $builder = new MyPDOBuilder($config);
    $db = $builder->stalkerDb();

    if(!isset($conf['api_url']))
    {
        exit('Stalker Portal API URL must be set in config.ini');
    }

    $stalker = new StalkerPortal(new RestApi($conf['api_url'], $conf['api_login'], $conf['api_pass']));

    if($stalker->checkConnection())
    {
        $account = new Account();
        $account->setIp($ip);
        $account->setExpireDate($expireDate);
        $account->setLogin(generateUniqueLogin($stalker));
        $account->setPassword(mt_rand(000001, 999999));
        $account->setTariffPlan($conf['tariff_plan']);
        $account->setStatus(true);

        $post = $stalker->addAccount($account);

        if($post === true)
        {
            $db->update('users', ['ip' => $account->getIp(), 'login' => $account->getLogin()], 'login');

            $newUser = $stalker->getUserByLogin($account->getLogin());
            $tariff = $stalker->getTariffInfo($newUser['tariff_plan']);
            echo "New user successfully added to portal" . PHP_EOL .
                "login: " . $newUser['login'] . PHP_EOL .
                "password: " . $account->getPassword() . PHP_EOL .
                "ip: " . $newUser['ip'] . PHP_EOL .
                "status: " . $newUser['status'] . PHP_EOL .
                "tariff: " . $tariff['name'] . PHP_EOL .
                "expire date: " . $newUser['end_date'] . PHP_EOL;
        }
        else
        {
            echo "Account adding failed" . PHP_EOL;
        }
    }
    else
    {
        exit('No connection to Stalker Portal API server ' . PHP_EOL);
    }
}
catch(Exception $e)
{
    exit($e->getMessage());
}

exit;


?>