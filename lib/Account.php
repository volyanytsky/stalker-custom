<?php

class Account extends StalkerPortalAccount
{
    /**
     * @var string
     */
    private $login;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $accountNumber;
    /**
     * @var string
     */
    private $tariffPlan;
    /**
     * @var string
     */
    private $status;
    /**
     * @var string
     */
    private $mac;
    /**
     * @var string
     */
    private $comment;
    /**
     * @var string
     */
    private $expireDate;
    /**
     * @var string
     */
    private $accountBalance;

    private $ip;


    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $accountNumber
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @param mixed $tariffPlan
     */
    public function setTariffPlan($tariffPlan)
    {
        $this->tariffPlan = $tariffPlan;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = (boolean)$status;
    }

    /**
     * @param mixed $mac
     * @throws Exception
     */
    public function setMac($mac)
    {
        if(!filter_var(strtoupper($mac), FILTER_VALIDATE_MAC))
        {
            throw new Exception("Incorrect mac value", 1);
        }
        $this->mac = $mac;
    }

    /**
     * @param mixed $ip
     * @throws Exception
     */
    public function setIp($ip)
    {
        if(!filter_var($ip, FILTER_VALIDATE_IP))
        {
            throw new Exception("Incorrect IP value", 1);
        }
        $this->ip = $ip;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @param mixed $expireDate
     * @throws Exception
     */
    public function setExpireDate($expireDate)
    {
        if(DateTime::createFromFormat("Y-m-d H:i:s", $expireDate) === false)
        {
            throw new Exception("Incorrect date format. STB expire date must be in Y-m-d H:i:s format", 1);
        }
        $this->expireDate = $expireDate;
    }

    /**
     * @param mixed $accountBalance
     */
    public function setAccountBalance($accountBalance)
    {
        $this->accountBalance = $accountBalance;
    }


    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return mixed
     */
    public function getTariffPlan()
    {
        return $this->tariffPlan;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }


    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return mixed
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * @return mixed
     */
    public function getAccountBalance()
    {
        return $this->accountBalance;
    }


    public function getFullName()
    {
        return $this->getName();
    }

    public function getStbMac()
    {
        return $this->getMac();
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->getExpireDate();
    }
}