<?php

class MyPDOBuilder
{
    private $config;
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function stalkerDb()
    {
        $credits = $this->getCredits();

        return new MyPDO($credits['host'], $credits['name'], $credits['user'], $credits['pass']);
    }

    private function getCredits()
    {
        $credits = [];

        $conf = $this->config->get();
        if(!isset($conf['stalker_portal_dir']) || empty($conf['stalker_portal_dir']))
        {
            throw new ConfigException("Config exception: stalker_portal_dir has not been defined");
        }

        ($conf['stalker_portal_dir'][strlen($conf['stalker_portal_dir'] - 1)] === '/') ?
            $stalkerPath = $conf['stalker_portal_dir'] :
            $stalkerPath = $conf['stalker_portal_dir'] . '/';

        $customIni = null;
        if(file_exists($stalkerPath . 'server/custom.ini'))
        {
            $customIni = parse_ini_file($stalkerPath . 'server/custom.ini');
        }

        $configIni = null;
        if (file_exists($stalkerPath . 'server/config.ini'))
        {
            $configIni = parse_ini_file($stalkerPath . 'server/config.ini');
        }
        else
        {
            throw new ConfigException("stalker_portal/server/config.ini does not exist. Your portal does not seem to be working at all");
        }

        (isset($customIni['mysql_host'])) ? $credits['host'] = $customIni['mysql_host'] : $credits['host'] = $configIni['mysql_host'];
        (isset($customIni['mysql_user'])) ? $credits['user'] = $customIni['mysql_user'] : $credits['user'] = $configIni['mysql_user'];
        (isset($customIni['mysql_pass'])) ? $credits['pass'] = $customIni['mysql_pass'] : $credits['pass'] = $configIni['mysql_pass'];
        (isset($customIni['db_name'])) ? $credits['name'] = $customIni['db_name'] : $credits['name'] = $configIni['db_name'];

        return $credits;
    }
}