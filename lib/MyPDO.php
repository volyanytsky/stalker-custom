<?php

class MyPDO
{
    private $type;
    private $host;
    private $name;
    private $user;
    private $pass;
    private $charset;
    private $options;

    public function __construct($host, $name, $user, $pass, $type = 'mysql', $charset = 'utf8')
    {
        $this->type = $type;
        $this->host = $host;
        $this->name = $name;
        $this->charset = $charset;
        $this->user = $user;
        $this->pass = $pass;
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];
    }

    private function __clone() {}

    public function getInstance()
    {
        if(self::$instance === null)
        {
            return new self();
        }
        return self::$instance;
    }

    private function connect()
    {
        $dsn = "$this->type:host=$this->host;dbname=$this->name;charset=$this->charset";
        return new PDO($dsn, $this->user, $this->pass, $this->options);
    }

    public function delete($table, $key, $value)
    {
        if(!$this->tableExists($table))
        {
            throw new MyPDOException("Update error: $table does not exist");
        }
        $sql = "DELETE FROM `$table` WHERE `$key` = :$key";
        $this->execute($sql, self::serializeData([$key => $value]));
    }

    public function insert($table, array $data)
    {
        if(!$this->tableExists($table))
        {
            throw new MyPDOException("Update error: $table does not exist");
        }
        $columns = implode(",", array_keys($data));
        $values = [];
        foreach($data as $index => $value)
        {
            $values[] = ":".$index;
        }
        $values = implode(",", $values);
        $sql = "INSERT INTO `$table`($columns) VALUES($values)";
        $this->execute($sql, self::serializeData($data));
    }

    public function update($table, array $data, $key = 'id')
    {
        if(!$this->tableExists($table))
        {
            throw new MyPDOException("Update error: $table does not exist");
        }
        if(!isset($data[$key]))
        {
            throw new MyPDOException("Unable to update $table: data array does not contain $key");
        }
        $sql = "UPDATE `$table` SET ";
        foreach(array_keys($data) as $index)
        {
            if($index != $key)
            {
                $sql .= "$index = :$index,";
            }
        }
        $sql = rtrim($sql, ",");
        $sql .= " WHERE $key = :$key";

        $values = self::serializeData($data);

        $this->execute($sql, $values);
    }

    public function insertOrUpdate($table, array $data, array $keys = ['id'])
    {
        if(!$this->tableExists($table))
        {
            throw new MyPDOException("Update error: $table does not exist");
        }
        $columns = implode(",", array_keys($data));
        $values = [];
        foreach($data as $index => $value)
        {
            $values[] = ":".$index;
        }
        $values = implode(",", $values);
        $sql = "INSERT INTO `$table`($columns) VALUES($values) ON DUPLICATE KEY UPDATE ";
        foreach(array_keys($data) as $column)
        {
            if(!in_array($column, $keys))
            {
                $sql .= "$column=VALUES($column),";
            }
        }
        $sql = rtrim($sql,",");
        $this->execute($sql, self::serializeData($data));
    }

    public function execute($query, $args = [])
    {
        $stn = $this->connect()->prepare($query);
        return $stn->execute($args);
    }

    public function fetch($query, $args = [])
    {
        $request = $this->connect()->prepare($query);
        $request->execute($args);
        return $request->fetch();
    }

    public function fetchAll($query, $args = [])
    {
        $request = $this->connect()->prepare($query);
        $request->execute($args);
        return $request->fetchAll();
    }

    public function fetchColumn($query, $args = [])
    {
        $request = $this->connect()->prepare($query);
        $request->execute($args);
        return $request->fetchColumn();
    }

    static public function createPlaceholders(array $args)
    {
        $placeholders = [];
        for($i = 0; $i < count($args); $i++)
        {
            $placeholders[$i] = '?';
        }
        return implode(",", $placeholders);
    }

    protected function tableExists($table)
    {
        $data = $this->fetchAll("SHOW TABLES FROM ".$this->name);
        $tables = array_column($data, 'Tables_in_'.$this->name);
        (in_array($table, $tables)) ? $res = true : $res = false;
        return $res;
    }

    static private function serializeData($data)
    {
        $serialized = [];
        foreach($data as $key => $value)
        {
            $serialized[":".$key] = $value;
        }
        return $serialized;
    }
}

?>