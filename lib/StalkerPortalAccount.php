<?php

/**
 * @author Sergey Volyanytsky <sergey.volyanytsky@gmail.com>
 * @link https://wiki.infomir.eu/eng/stalker/stalker-setup-guide/rest-api-v1#RESTAPIv1-ACCOUNTS
 */
abstract class StalkerPortalAccount implements ResourceFieldsInterface
{
    /**
     * @return string
     */
    abstract public function getLogin();

    /**
     * @return string
     */
    abstract public function getPassword();

    /**
     * @return string
     */
    abstract public function getFullName();

    /**
     * @return string
     */
    abstract public function getAccountNumber();

    /**
     * @return string
     */
    abstract public function getTariffPlan();

    /**
     * @return string
     */
    abstract public function getStatus();

    /**
     * @return string
     */
    abstract public function getStbMac();

    /**
     * @return string
     */
    abstract public function getComment();

    /**
     * @return string
     */
    abstract public function getEndDate();

    /**
     * @return string
     */
    abstract public function getAccountBalance();

    /**
     * @return array
     * @throws StalkerPortalApiExeption
     */
    final public function getData()
    {
        if(!$this->getLogin())
        {
            throw new StalkerPortalApiExeption("Login is the required field");
        }

        $data = [];
        $data['login'] = $this->getLogin();

        if($this->getPassword())
        {
            $data['password'] = $this->getPassword();
        }

        if($this->getFullName())
        {
            $data['full_name'] = $this->getFullName();
        }

        if($this->getAccountNumber())
        {
            $data['account_number'] = $this->getAccountNumber();
        }

        if($this->getTariffPlan())
        {
            $data['tariff_plan'] = $this->getTariffPlan();
        }

        $data['status'] = $this->getStatus();

        if($this->getStbMac())
        {
            $data['stb_mac'] = $this->getStbMac();
        }

        if($this->getComment())
        {
            $data['comment'] = $this->getComment();
        }

        if($this->getAccountBalance())
        {
            $data['account_balance'] = $this->getAccountBalance();
        }

        if($this->getEndDate())
        {
            $data['end_date'] = $this->getEndDate();
        }

        return $data;
    }
}