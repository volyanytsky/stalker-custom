<?php

/**
 * @author Sergey Volyanytsky <sergey.volyanytsky@gmail.com>
 */
interface RestInterface
{
    public function get($resource, $id = '');

    /**
     * @param $resource
     * @param array $data
     * @return mixed
     */
    public function post($resource, array $data);

    /**
     * @param $resource
     * @param array $data
     * @return mixed
     */
    public function put($resource, array $data);

    /**
     * @param $resource
     * @param $id
     * @return mixed
     */
    public function delete($resource, $id);
}