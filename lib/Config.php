<?php

class Config
{
    private $path;

    /**
     * Config constructor.
     * @param $path
     */
    public function __construct($path = null)
    {
        if($path === null)
        {
            if(file_exists('./config.ini'))
            {
                $this->path = './config.ini';
            }
            elseif(file_exists('../config.ini'))
            {
                $this->path = '../config.ini';
            }
            else
            {
                throw new ConfigException("Config file does not exist in default location");
            }
        }
        elseif(file_exists($path))
        {
            $this->path = $path;
        }
        else
        {
            throw new ConfigException("Config file does not exist at " . $path);
        }

    }


    public function get()
    {
        return parse_ini_file($this->path);
    }
}


?>