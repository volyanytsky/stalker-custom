<?php

class StalkerPortal
{
    private $api;

    public function __construct(RestInterface $api)
    {
        $this->api = $api;
    }

    public function addAccount(StalkerPortalAccount $account)
    {
        $data = $this->api->post('accounts', $account->getData());
        return $this->decodeAnswer($data);
    }

    public function getUserByLogin($login)
    {
        return $this->decodeAnswer($this->api->get('users', $login));
    }

    public function getTariffInfo($id)
    {
        $tariffs = $this->decodeAnswer($this->api->get('tariffs'));
        foreach ($tariffs as $tariff)
        {
            if($tariff['external_id'] == $id)
            {
                return $tariff;
            }
        }
        return null;
    }

    public function checkConnection()
    {
        try
        {
            $data = $this->api->get("");
            $res = $this->decodeAnswer($data);
        }
        catch(StalkerPortalApiExeption $e)
        {
            if($e->getMessage() === 'Empty resource')
            {
                return true;
            }
            throw new Exception($e->getMessage());
        }

        return false;
    }

    public function isLoginUnique($login)
    {
        try
        {
            $res = $this->api->get('users', $login);
            $data = $this->decodeAnswer($res);
        }
        catch(StalkerPortalApiExeption $e)
        {
            if($e->getMessage() === "Account not found")
            {
                return true;
            }
        }
        return false;
    }

    private function decodeAnswer($jsonString)
    {
        $answer = json_decode($jsonString, true);

        if($answer['status'] === 'OK')
        {
            return $answer['results'];
        }

        if($answer['status'] === 'ERROR')
        {
            throw new StalkerPortalApiExeption($answer['error']);
        }

        return null;
    }
}